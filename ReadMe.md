# Site web de la JSI

La Jeunesse Sportive est un club associatif omnisport basée à Irigny qui réunit 10 sections sportives 

## Structure
Les textes de section sont à rédiger dans les *page-section* afférentes, situées dans le dossier `contenu/`.  
Les images à placer dans `contenu/sections/image`

Chaque page est divisée en deux parties. 

Une première, dites *Frontmatter*, est reconnaissable à sa syntaxe aride. Une étiquette, deux points, une valeur. 
Le *Frontmatter* est introduit et conclus par trois tirets `---`.

Ensuite, vient un texte libre rédigé en [Markdown](https://docs.framasoft.org/fr/grav/markdown.html).


## Edition

Tout changement dans les fichiers `.md` présents dans le dossier contenu induit une mise à jour du site.

### Ajouter une adresse Web à une page section

Par exemple, pour le site de la section Bilboquet, `jsi-bilboquet.fr`, il suffit d'ajouter dans le *Frontmatter* et après la mention `web`, l'adresse du site. 👇

`web: jsi-bilboquet.fr`

Si le besoin d'ajouter une adresse web autre que celle "officielle" de la section, il convient de l'ajouter dans le champ de texte libre. ([Voir exemple plus bas](#Ajouter-un-lien-vers-un-document-PDF-hébergé-ailleurs))

### Couleurs

Par défaut, les pages de section s'affichent en vert.

Plusieurs couleurs sont possibles :

+ vert
+ rouge
+ jaune
+ bleu

Les pages s'afficheront texte foncé sur fond de couleur claire.
Il est possible d'inverser ce comportement en ajoutant l'appendice `-sombre` à la couleur. 👉 `rouge-sombre` 

Par exemple, pour changer la couleur de fond de la section Bilboquet en rouge, il suffit d'ajouter dans le *Frontmatter* et après la mention `couleur`, la couleur désirée. 👇

`couleur: rouge` 

Pour obtenir une page texte claire sur fond foncée 👇

`couleur: rouge-sombre` 


### Ajouter un réseau social à une page section

Il est possible d'ajouter un lien vers :

+ facebook
+ twitter
+ tiktok
+ instagram



Pour cela, il convient d'obtenir l'identifiant de l'entité à inclure, cela quelque soit le reseau social.

Pour facebook, regardez l'adresse de la page officiel et prenez l'expression juste après `facebook.com/` et juste avant un éventuel `\` 

facebook.com/*JSI-Bilboquet*

Pour Twitter :

twitter.com/*JSI-Bilboquet*

Pour Instagram :

instagram.com/*JSI-Bilboquet*

Cette expression représente l'identifiant de la page

Rendez-vous dans la *page-section*, dans le *frontmatter* à la ligne social.

Pour inclure un lien facebook, vous pouvez copier-coller l'expression suivante, en remplaçant `JSI-Bilboquet` par votre identifiant

`social: [["facebook", "JSI-Bilboquet"]]`

Pour ajouter deux liens : 

`social: [["facebook", "JSI-Bilboquet"], ["instagram", "JSI-Bilboquet"]]`

Notez que chaque lien est composé de deux entrées : une étiquette, une virgule, une valeur. Cet ensemble est prit entre deux crochets, éventuellement suivit d'un autre lien suivant la même logique.
Cet ensemble est enfin inclus au sein de deux derniers crochets.


### Ajouter un lien vers un document PDF hébergé ailleurs

Chaque section peut inclure des ressources externes dans sa page. Il est préférable, pour garantir l'autonomie d'édition et éviter les allers-retours inutiles, que chaque section garde une capacité d'édition entière et directe sur son propre document.

À cet effet, la section peut :
+ héberger un document PDF sur un Google Drive ([Tutoriel](https://www.youtube.com/watch?v=BiD9nSfYASU)) et nous donner le lien
+ Créer un article sur leur site et nous donner le lien
+ créer un [Framapad](https://annuel.framapad.org), un outil de traitement de texte collaboratif et nous donner le lien

Ainsi, il ne nous reste plus qu'à intégrer, sur la page de section du site, l'expression :

`[Lire le règlement Bla Bla Bla ](www.lien-vers-le-document-blabla.fr)`




## Design

### Images

Les images sont détourées et tramées.

#### Comment reproduire l'effet ?

+ Détourer l'image.
+ La placer dans un format de 1000x1000px
+ Appliquer une Trame de Demi-Teinte (linéature autour de 10/15 ligne/pouce, angle 45°, forme Cercle
+ Exporter pour le Web en réduisant les couleurs au nombre de 3 afin d'obtenir une image d'environ 30kb.

### Developpement
ELEVENTY_ENV=dev npx eleventy --serve 
→ localhost:8080

### CSS 
Le SCSS n'est généré qu'en mode developpement. Pensez à dubliquer `main.css` du dossier `public` à `design/assets/css`