---
sport: judo
nom: JSI Judo

adresse: Dojo - 21 rue du stade<br> 69540 Irigny

web : jsi-judo.com
mail: jsi.judo@gmail.com
social:

fondation: 1960
federation: FFJDA
president: Edwige JADOT
tresorier: Bernard Taralle
secretaire: Marie-Christine Taralle

adherent: 59
couleur: rouge-sombre
---

La section Judo est la première section créée par la Jeunesse Sportive d’Irigny.

Elle est affiliée à la Fédération Française de Judo. 

Le club à taille humaine, met l’accent sur les jeunes et la compétition. 

Les cours sont assurés par un professeur diplômé d’état 1er degré – ceinture noire 2ème dan – champion de France 1990.


