---
sport: aikido 
nom: JSI Aikido

adresse: Dojo - 21 rue du stade<br>69540 Irigny

web: aikido-irigny.fr
mail: levasseur148@yahoo.fr
social: 

fondation: 1985
federation: FFAB
president: Philippe Levasseur
tresorier: Gisèle Bost
secretaire: Nicolas Ralph

adherent: 24
couleur: rouge
---

L'AÏKIDO est un art martial japonais créé par Moribei Uesbiba Ô Sensei vers la fin des années 1940. C'est un art martial efficace avec des techniques visant à canaliser et à contrôler l'adversaire en utilisant son énergie pour la retourner contre lui-même.

La section d'IRIGNY transmet la pratique de Maître 1i\MURA élève direct de Uesbiba Morihei.
 
Elle est affiliée à la FFAB (Fédération Fançaise d'Aïkido et de Budo agrée par le Ministère des sports et reconnue d'utilité publique)
