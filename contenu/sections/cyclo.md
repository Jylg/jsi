---
sport: cyclo
nom: JSI Cyclo

adresse: 17 rue de la Visina<br>69540 Irigny

web: jsicyclo.asso-web.com
mail: cyclojsimanu@gmail
social: 

fondation: 2002
federation: FFCT
president: Manuel Salazar
tresorier: Eric Hache
secretaire: Gérard Léger


adherent: 51
couleur: bleu
---

Le club est né en septembre 2002 grâce notamment à la volonté de 2 personnes : Yves Brun et Jean-Luc Fauvain. C'est un jeune club de 18 ans installé à Irigny.
Nous sommes une soixantaine de membres à pratiquer la bicyclette sur route, et nous sommes affiliés à la Fédération Française de Cyclotourisme (FFCT). Au sein du CODEP 69, nous nous situons dans le premier tiers des clubs en terme de nombre d'adhérents.
 
Les membres de la section viennent des différentes communes qui entourent Irigny : Lyon, Villeurbanne, Millery, Charly, Vourles, Vernaison, Saint Genis Laval, Pierre-Bénite, Grigny, Oullins, Bron, Chaponost, Communay et même Chazay d'Azergues ! Sans oublier Irigny pour un peu moins de la moitié.

La section n'est pas très jeune en terme de moyenne d'âge, mais sait se montrer particulièrement dynamique sur le vélo comme dans les autres activités et animations qu'il crée ou auxquelles il participe.
Si la passion du vélo nous réunit 3 ou 4 fois par semaine (voir le site) de manière très régulière et hebdomadaire, et ce toute l'année, la section a à coeur de proposer à ses adhérents de nombreuses sorties et animations : participation aux rallyes "du coin" ou plus éloignés (Ardéche ou Drôme par exemple), sorties du week end à la montagne ou sur d'autres terrains en famille, semaine vélo annuelle à l'étranger ou en métropole, randonnées pédestres en famille avec ou sans raquettes, tournoi de boules, galette et beaujolais, etc ...

Si le vélo est au cœur de la motivation de chaque membre, la cohésion, l'ambiance, l'esprit de camaraderie, la bonne humeur et la convivialité sont recherchés par tous.
Très récemment, la section cyclo s'est aussi adapté aux besoins et envies de ses membres qui voulaient passer sur VAE pour raisons personnelles. Cette cohabitation se fait sans aucune difficulté et naturellement et comble chacun, quels que soient son envie et son niveau.

Enfin, notre section fait preuve de sa vivacité et de sa jeunesse en organisant un rallye depuis 16 ans, "l'Irignoise", offrant plusieurs possibilités de parcours aux cyclistes route comme VTT ou encore randonneurs.

J'espère que vous trouverez toutes les informations utiles dans ce site et bien d'autres et si l'envie vous prend, rejoignez-nous! Vous y serez bien accueillis.

Manuel Salazar Président de la JSI Cyclo"
