---
sport: karaté
nom: JSI Karaté

adresse: Dojo - 21 rue du stade<br>69540 Irigny

web: 
mail: alexia.tanlet@aliceadsl.fr
social: [["facebook", "jsikaratedo"]]

fondation: 1985 
federation: FFKAMA
president: Alexia Tanlet
tresorier: Marie-Laure Tanlet
secretaire: Quentin Tanlet

adherent: 55
couleur: rouge-sombre

---

La section karaté pratique le karaté style shotokan.
Ouvert à tous : petits et grands à partir de 6 ans - pour débutants, comme pour confirmés.

Horaires : les mardis et vendredis 

de 19h à 20h pour les enfants,  
de 20h15 à 22h00 pour les adultes.

N'hésitez pas à nous contacter pour plus de renseignements.
