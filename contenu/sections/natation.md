---
sport: natation
nom: JSI natation

adresse: Piscine - chemin de Champvillard<br>69540 Irigny

web: jsi-natation.fr
mail: jsi_natation@yahoo.fr
social: [["facebook", "jsi.natation"]]

fondation: 1980
federation: UFOLEP
president: René Canuto
tresorier: Frédérique Gauthier
secretaire: Stéphanie Cogoli

adherent: 94
couleur: bleu
---
La JSI Natation est un club vaillant qui cultive une tradition d'excellence qu'il s'agisse d'une pratique de formation, de compétition ou de loisir , le club mise sur la qualité.

La natation est une activité sportive et complète, développant le dépassement de soi .
C'est un sport individuel mais un état d'esprit collectif !

Fidèle à ses traditions la section natation de la JSI prône une approche à la fois éducative et compétitive .

Un objectif : nous sommes tous concentrés sur notre mission afin de permettre aux nageurs de donner le meilleur.

 
